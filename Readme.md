## Nötige Software

1. `nvm-setup.zip` auf <https://github.com/coreybutler/nvm-windows/releases> herunterladen und installieren
2. VS Code from <https://code.visualstudio.com/download> herunterladen und installieren
3. Optional: `git` von <https://git-scm.com/download/win> herunterladen und installieren


## Neues Projekt starten

1. `git clone https://angelozehr@bitbucket.org/angelozehr/rsi-map.git new-project`
2. Ordner `new-project` in VS Code öffnen
3. Terminal unten öffnen
4. `npm install` eingeben
5. `npm install -g gulp`
6. dann `gulp`
7. Beenden mit `Ctrl + C`
8. oder statt `gulp` einfach `gulp build`

Ordner `build` per FTP auf CDN hochladen.
Per iFrame in Artikel einbinden.

## Neue Gemeindegrenzen erstellen

1. Neues Shape-File herunterladen unter <https://m4.ti.ch/dfe/de/ucr/documentazione/download-file/>
2. Unzip und alle Dateien hochladen bei <http://mapshaper.org/>
3. Simplify > Prevent Shape removal > 4.5%
4. Export GeoJSON `geo/comuni_geo.json` und TopoJSON als `geo/comuni_topo.json`
5. Manuell nach `"FeatureCollection",` folgenden Code einfügen: `"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::21781" } },`

