/* global L, proj4, d3 */
// definitions needed for swiss coordinate system to work
var res = [3000, 2750, 2500, 2250, 2000, 1750, 1500, 1250, 1000, 750, 650, 500, 400, 300, 200, 175, 150, 125, 100, 75, 50, 25, 15, 10, 5, 2.5, 2, 1.5, 1, 0.5]
var orig = [420000, 350000]
var scale = function (zoom) { return 1 / res[zoom] }
var epsg = 'EPSG:21781'
var proj = '+proj=somerc +lat_0=46.95240555555556 +lon_0=7.439583333333333 +k_0=1 +x_0=600000 +y_0=200000 +ellps=bessel +towgs84=674.374,15.056,405.346,0,0,0,0 +units=m +no_defs'
d3.formatDefaultLocale({ 'decimal': ',', 'thousands': '\'', 'grouping': [3], 'currency': ['', '\u00a0CHF'] })

// define what to do on window resize
function onResize () {
  document.getElementById('map').style.width = `${window.innerWidth}px`
  document.getElementById('map').style.height = `${window.innerHeight - 30}px`
}

// execute once
onResize()

// listen for change
window.addEventListener('resize', onResize)

// create new coordinate system (swiss style CH1903)
var crs = new L.Proj.CRS(epsg, proj, {
  resolutions: res,
  origin: orig
})

// set outer bounds: Ticino
var boundsTicino = L.latLngBounds([
  [46.634667, 8.306765],
  [45.798490, 9.192538]
])

// initialize new map
var map = L.map('map', {
  crs: crs,
  continuousWorld: true,
  worldCopyJump: false,
  scale: scale
})

// zoom to Ticino
map.fitBounds(boundsTicino)

proj4.defs(epsg, proj)

// load geodata and data
L.Util.ajax('comuni_geo.json').then(function (geodata) {
  L.Util.ajax('data.json').then(function (data) {
    // define min and max of data
    const extent = d3.extent(data, d => +d.value)

    // initialize new color scale
    const colorScale = d3.scaleSequential(d3.interpolateViridis)
      .domain(extent)

    // Add a legend for the color values.
    var svg = d3.select('#legend')
    svg.attr('width', window.innerWidth)
    var legend = svg.selectAll('g.legend')
      .data(colorScale.ticks(5).reverse())
    .enter().append('g')
      .attr('class', 'legend')
      .attr('transform', function (d, i) { return `translate(${10 + i * 120})` })

    legend.append('rect')
      .attr('width', 20)
      .attr('height', 20)
      .style('fill', colorScale)

    legend.append('text')
      .attr('x', 26)
      .attr('y', 10)
      .attr('dy', '.35em')
      .text(d3.format(','))

    // draw municipalities as geojson features
    L.Proj.geoJson(geodata, {
      style: function (feature) {
        // find entry in data.json for current geojson feature
        const entry = data.find(d => +d.id === +feature.properties.OBJECTID)

        // calculate fill color
        const color = entry.color ? entry.color : colorScale(+entry.value)

        return {
          'fillColor': color,
          'fillOpacity': 0.65,
          'fill': true,
          'color': '#FFFFFF',
          'weight': 1,
          'opacity': 0.65
        }
      },
      onEachFeature: function (feature, layer) {
        if (feature.properties) {
          // find entry in data.json for current geojson feature
          const entry = data.find(d => +d.id === +feature.properties.OBJECTID)

          // define content of popup
          const content = `<h5>${entry.name}</h5><p>${entry.key}: ${entry.value}</p>`

          // add popup on mouseover
          layer.bindPopup(content, {closeButton: false, offset: L.point(0, -20), className: 'srf-tooltip'})
          layer.on('mouseover', function () { layer.openPopup() })
          layer.on('mouseout', function () { layer.closePopup() })
        }
      }
    }).addTo(map)
  })
})
