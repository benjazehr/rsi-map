const gulp = require('gulp')
const connect = require('gulp-connect')
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer')
const csv2json = require('gulp-csv2json')
const rename = require('gulp-rename')
const babel = require('gulp-babel')
const sourcemaps = require('gulp-sourcemaps')

var libSources = [
  './node_modules/leaflet/dist/leaflet.js',
  './node_modules/leaflet-ajax/dist/leaflet.ajax.min.js',
  './node_modules/proj4leaflet/lib/proj4-compressed.js',
  './node_modules/proj4leaflet/src/proj4leaflet.js',
  './node_modules/d3/build/d3.min.js'
]
var jsSources = ['script.js']
var cssSources = ['./node_modules/leaflet/dist/leaflet.css', '*.css']
var htmlSources = ['*.html']
var csvSources = ['*.csv']
var outputDir = 'build'

gulp.task('html', function () {
  gulp.src('index.html')
  .pipe(gulp.dest(outputDir))
})

gulp.task('css', function () {
  gulp.src(cssSources)
  .pipe(autoprefixer({
    browsers: ['last 2 versions'],
    cascade: false
  }))
  .pipe(concat('css.css'))
  .pipe(gulp.dest(outputDir))
  .pipe(connect.reload())
})

gulp.task('lib', function () {
  gulp.src(libSources)
  .pipe(concat('lib.js'))
  .pipe(gulp.dest(outputDir))
})

gulp.task('js', function () {
  gulp.src(jsSources)
  .pipe(sourcemaps.init())
  .pipe(babel({
    presets: ['env']
  }))
  .pipe(concat('js.js'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(outputDir))
  .pipe(connect.reload())
})

gulp.task('fonts', function () {
  gulp.src('fonts/**')
  .pipe(gulp.dest(outputDir))
})

gulp.task('geo', function () {
  gulp.src('geo/**')
  .pipe(gulp.dest(outputDir))
})

gulp.task('csv', function () {
  gulp.src(csvSources)
  .pipe(csv2json({}))
  .pipe(rename({extname: '.json'}))
  .pipe(gulp.dest(outputDir))
  .pipe(connect.reload())
})

gulp.task('watch', function () {
  gulp.watch(jsSources, ['js'])
  gulp.watch(cssSources, ['css'])
  gulp.watch(htmlSources, ['html'])
  gulp.watch(csvSources, ['csv'])
})

gulp.task('connect', function () {
  connect.server({
    root: 'build',
    livereload: true
  })
})

gulp.task('default', ['html', 'lib', 'js', 'css', 'fonts', 'geo', 'csv', 'connect', 'watch'])
gulp.task('build', ['html', 'lib', 'js', 'css', 'fonts', 'geo', 'csv'])
